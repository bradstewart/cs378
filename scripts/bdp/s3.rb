
require 'uuid'

module BDP
  class S3

    def initialize
      @s3 = AWS::S3.new
    end

    def bucket
      @s3.buckets['utcs378']
    end

    def jars
      objects('jars')
    end

    def output(name)
      objects("output/#{name}")
    end

    def upload_jar
      path = File.expand_path('../cs378/out/artifacts/cs378_jar/cs378.jar')
      File.open(path) do |file|
        name = File.basename(file)
        puts name
        get_jar(name).write(file)
      end
    end

    def clean_output(dataset)
      new_prefix = "bjs2639/previous_outputs/#{dataset}/#{unique_id}"
      output(dataset).each do |obj|
        puts obj.key
        name = file_name_for(obj.key)
        puts name
        obj.move_to("#{new_prefix}/#{name}")
      end
    end

    private

    def previous_output_path(dataset)
      prefix("output/#{dataset}/#{unique_id}")
    end

    def file_name_for(key)
      key.split('/')[-1]
    end

    def unique_id
      UUID.new.generate
    end

    def get_jar(name)
      bucket.objects["#{prefix('jars')}/#{name}"]
    end

    def objects(prefix)
      bucket.objects.with_prefix(prefix(prefix))
    end

    def prefix(prefix)
      "bjs2639/#{prefix}"
    end

  end
end
