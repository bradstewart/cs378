module BDP
  class EMR

    def default_steps(options = {})
      [{
        name: 'default_jar_step',
        action_on_failure: 'CONTINUE',
        :hadoop_jar_step => {
          jar: jar_url(options[:name] || 'cs378.jar'),
          :args => ['s3n://utcs378/data/dataSet1.txt', 's3n://utcs378/bjs2639/output/dataSet1']
        }
      }]
    end

    def initialize
      @emr = AWS::EMR.new
      @client = @emr.client
    end

    def job_flows
      @emr.job_flows
    end

    def run_job
      @client.run_job_flow(default_job_flow)
    end

    def add_step
      @emr.job_flows['job-flow-id'].add_steps()
    end

    private

    def jar_url(name)
      "s3://utcs378/bjs2639/jars/#{name}"
    end

    def default_job_flow
      {
        name: 'bjs2639',
        log_uri: 's3://utcs378/bjs2639/logs/',
        :instances => {
          :instance_count => 2,
          :master_instance_type => 'm1.small',
          :slave_instance_type => 'm1.small',
          termination_protected: false,
        },
        steps: default_steps,
        visible_to_all_users: false
      }
    end

  end
end