package com.refactorlabs.cs378.hw9;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.refactorlabs.cs378.hw7.ImpressionTimestampComparator;
import com.refactorlabs.cs378.hw8.SessionCategorizer;
import com.refactorlabs.cs378.sessions.Impression;
import com.refactorlabs.cs378.sessions.ImpressionType;
import com.refactorlabs.cs378.sessions.Lead;
import com.refactorlabs.cs378.sessions.Session;
import org.apache.avro.mapred.AvroValue;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Reduce class combining the outputs from the ImpressionMapper and LeadMapper into a single Session object.
 * An individual Lead's vdp_index is set to the index of the matching Impression in the Impression array,
 * where matching is determined by Id. If no match is found, it defaults to -1. Output is written into
 * multiple files named for the category of the session.
 *
 * @author Brad Stewart. Created on 11/12/14.
 */
public class SessionReducer extends Reducer<Text, AvroValue<Session>, Text, Text> {

    protected SessionFilter filter;
    protected MultipleOutputs multipleOutputs;

    /**
     * Setup the multipleOutputs before running reduce.
     *
     * @param context
     */
    @Override
    public void setup(Context context)  {
        log("hello");
        System.out.println("hello");
        System.err.println("hello");

        multipleOutputs = new MultipleOutputs(context);

        String str = context.getConfiguration().get("filter_percentage");
        log("filter_percentage is "+str);
        System.err.println("filter_percentage is "+str);
        filter = new SessionFilter(Double.parseDouble(str));
    }

    /**
     *
     * @param key           Session.user_id:Session.api_key
     * @param values        Avro Session object list
     * @param context
     * @throws java.io.IOException
     * @throws InterruptedException
     */
    @Override
    public void reduce(Text key, Iterable<AvroValue<Session>> values, Context context)
            throws IOException, InterruptedException {

        // TreeSet to sort the combined list
        Set<Impression> impressions = Sets.newTreeSet(new ImpressionTimestampComparator());
        Set<Lead> leads = Sets.newTreeSet();

        Session session = null;
        boolean isImpression = false;

        for( AvroValue<Session> value : values) {
            Session s = value.datum();

            // We only need the session data from one object
            if (session == null) {
                session = Session.newBuilder(value.datum()).build();
            }

            // But the Impression related Sessions have a lot more data, so if we have
            // one of those, use it for the session.
            if (!isImpression && s.getImpressions().size() > 0) {
                isImpression = true;
                session = Session.newBuilder(value.datum()).build();
            }

            //Defensive copy this Session's impression and lead lists into the combined set
            impressions.addAll(ImmutableList.copyOf(value.datum().getImpressions()));
            leads.addAll(ImmutableList.copyOf(value.datum().getLeads()));
        }

        // Map impression ids to its index
        Map<Long, Integer> impressionIds = mapImpressionIds(impressions);

        // Assign vdp_index values based on the map
        for (Lead lead : leads) {
            Long id = lead.getId();
            Integer index = -1;

            if (impressionIds.containsKey(id))
                index = impressionIds.get(id);

            lead.setVdpIndex(index);
        }

        session.setImpressions(Lists.newArrayList(impressions));
        session.setLeads(Lists.newArrayList(leads));

        String category = SessionCategorizer.categorize(session);

        if (filter.include(session, category)) {
            multipleOutputs.write("userType", key, new Text(session.toString()), category );
        }

    }

    /**
     * Close the multipleOutputs after running reduce.
     *
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        multipleOutputs.close();
    }

    /**
     * Maps a Set of Impressions to a Hash with the keys set to its Id and value its index in the
     *  Set/Array. Only VDP Impressions occuring before a THANK_YOU are are added to the map.
     *
     * @param impressions   Set of impressions
     *
     * @return  Hash<Id, Index>
     */
    protected Map<Long, Integer> mapImpressionIds( Set<Impression> impressions ) {
        Map<Long, Integer> impressionIds = Maps.newHashMap();

        int i = 0;
        for (Impression impression : impressions) {

            if (impression.getImpressionType().equals(ImpressionType.VDP)) {
                for(Long id : impression.getId()) {
                    impressionIds.put(id, i);
                }
            } else if (impression.getImpressionType().equals(ImpressionType.THANK_YOU)) {
                break;
            }
            i++;
        }

        return impressionIds;
    }

    private void log(String message) {
        Logger.getLogger("").log(Level.SEVERE, "SessionReducer -- "+message);
    }
}