package com.refactorlabs.cs378.hw11;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Custom input format which generates random messages from a supplied list of words.
 *
 * @author Brad Stewart. Created on 12/2/14.
 */
public class RandomInputFormat extends InputFormat<LongWritable, Text> {

    public static String NUM_MAP_TASKS = "random.generator.map.tasks";
    public static String NUM_RECORDS_PER_TASK = "random.generator.num.records.per.task";

    @Override
    public List<InputSplit> getSplits(JobContext jobContext) throws IOException, InterruptedException {

        // Get the number of map tasks
        int numSplits = jobContext.getConfiguration().getInt(NUM_MAP_TASKS, -1);
        ArrayList<InputSplit> splits = new ArrayList<InputSplit>();

        // Add a dummy split for each task
        for (int i = 0; i < numSplits; i++) {
            splits.add(new FakeInputSplit());
        }

        return splits;
    }

    @Override
    public RecordReader<LongWritable, Text> createRecordReader(InputSplit inputSplit, TaskAttemptContext taskAttemptContext)
            throws IOException, InterruptedException {

        RandomRecordReader recordReader = new RandomRecordReader();
        recordReader.initialize(inputSplit, taskAttemptContext);

        return recordReader;
    }

    // These methods are used to set the initial values from the job configuration.

    public static void setNumMapTasks(Job job, int i) {
        job.getConfiguration().setInt(NUM_MAP_TASKS, i);
    }

    public static void setNumRecordsPerTask(Job job, int i) {
        job.getConfiguration().setInt(NUM_RECORDS_PER_TASK, i);
    }
}
