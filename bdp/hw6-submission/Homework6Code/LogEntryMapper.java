package com.refactorlabs.cs378.hw6;

import com.google.common.base.*;
import com.google.common.collect.Lists;
import com.refactorlabs.cs378.sessions.*;
import org.apache.avro.mapred.AvroValue;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * WordStatsWithLength class for the UserSessions MR job. Processes a log entry file and creates Avro Submission
 * objects with a nested Impression object.
 *
 * @author Brad Stewart. Created on 10/14/14.
 */
public class LogEntryMapper extends Mapper<LongWritable, Text, Text, AvroValue<Session>> {

    private static Splitter s = Splitter.on(',');
    private Text sessionId = new Text();
    private LogEntryParser parser;

    /**
     * @param key       File location
     * @param value     Document Text
     * @param context
     */
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        // Parse the string into a hash
        parser = LogEntryParser.parse(value.toString());
        sessionId.set(sessionId());
        context.write(sessionId, new AvroValue<Session>(buildSession()));
    }

    /**
     * Constructs the sessionId from the uid/user_id and apikey/api_key fields.
     *
     * @return      SessionId string
     */
    private String sessionId() {
        return get("uid") + ":" + get("apikey");
    }

    /**
     * Create a Session from the fields parsed by the parser.
     *
     * @return      Built Session object
     */
    private Session buildSession() {
        Session.Builder builder = Session.newBuilder();
        builder.setApiKey( get("apikey") )
               .setResolution( get("res") )
               .setUserId( get("uid") )
               .setUserAgent( get("uagent") )
               .setActivex( Utils.getActiveX(get("activex")) )
               .setImpressions( buildImpressionList() );

        return builder.build();
    }

    /**
     * Build an Impression for this log entry and ad it as the only element to a list.
     *
     * @return  List containing one build Impression object
     */
    private List<Impression> buildImpressionList() {
        Impression.Builder builder = Impression.newBuilder();
        builder.setAb( get("ab") )
               .setAddress( get("address") )
               .setCity(get("city"))
               .setState(get("state"))
               .setZip(Utils.getInt(get("zip")))
               .setLat(Utils.getDouble(get("lat")))
               .setLon(Utils.getDouble(get("lon")))
               .setDomain(get("domain"))
               .setImpressionType(Utils.getImpressionType(get("type")))
               .setAction(Utils.getAction(get("action")))
               .setActionName(Utils.getActionName(get("action_name")))
               .setId(buildIdList())
               .setPhoneType(Utils.getPhoneType(get("phone_type")))
               .setStartIndex( Utils.getInt(get("start_index")) )
               .setTimestamp(Utils.getInt(get("timestamp")))
               .setTotal(Utils.getInt(get("total")))
               .setVertical(Utils.getVertical(get("vertical")));
                
        return Lists.newArrayList(builder.build());
    }

    /**
     * Builds a list of Longs from the comma-separated string in the id field.
     *
     * @return List of Longs (sorted)
     */
    private List<Long> buildIdList() {
        Set<Long> longIds = new TreeSet<Long>();

        for( String id : s.split(get("id")) ) {
            longIds.add( Utils.getLong(id) );
        }
        return Lists.newArrayList(longIds);
    }

    /**
     * Convenience method so I didn't have to type parser.get() so many times.
     *
     * @param k     String field name
     * @return      Stored value or default
     */
    private String get(String k) {
        return parser.get(k);
    }

}
