package com.refactorlabs.cs378;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.IOException;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Brad Stewart. Created on 9/19/14.
 */
public class InvertedIndex {

    public static class InvertedIndexMapper extends Mapper<LongWritable, Text, Text, Text> {

        // Regex pattern for matching email addresses
        private static final String EMAIL_PATTERN = "([\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Za-z]{2,4})";
        private static Pattern EMAIL = Pattern.compile(EMAIL_PATTERN);

        private static Splitter splitter = Splitter.on(CharMatcher.BREAKING_WHITESPACE)
                                                    .trimResults()
                                                    .omitEmptyStrings();
        private Text messageId = new Text();
        private Text email = new Text();

        /**
         *
         * @param key
         * @param value
         * @param context
         */

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            String line = value.toString();
            boolean foundMessageId = false;
            String currentHeader = "";

            Iterator<String> itr = splitter.split(line).iterator();

            while (itr.hasNext()) {
                String token = itr.next().trim();

                if (!foundMessageId && isMessageId(token))
                    messageId.set(itr.next().trim());
                else if (isHeader(token))
                    currentHeader = token;
                else if (isMeta(token))
                    break;

                Matcher m = EMAIL.matcher(token);
                if (m.find()) {
                    email.set(currentHeader+m.group(1));
                    context.write(email, messageId);
                }

            }

        }

        private boolean isMessageId(String token) {
            return token.equals("Message-ID:");
        }

        private boolean isHeader(String token) {
            return (token.equals("To:") || token.equals("From:") || token.equals("Bcc:") || token.equals("Cc:"));
        }

        private boolean isMeta(String token) {
            return (token.equals("X-To:") || token.equals("X-From:") || token.equals("X-bcc:") || token.equals("X-cc:"));
        }

    }

    public static class InvertedIndexReducer extends Reducer<Text, Text, Text, Text> {

        private static Joiner joiner = Joiner.on(",").skipNulls();

        @Override
        public void reduce(Text key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {

            // Put each value into a TreeSet to sort and remove duplicates
            TreeSet<String> sortedValues = new TreeSet<String>();

            for ( Text value : values ) {
                sortedValues.add(value.toString().trim());
            }

            Text result = new Text( joiner.join(sortedValues) );
            context.write(key, result);
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        String[] appArgs = new GenericOptionsParser(conf, args).getRemainingArgs();

        Job job = new Job(conf, "invertedindex");
        // Identify the JAR file to replicate to all machines.
        job.setJarByClass(InvertedIndex.class);

        // Set the output key and value types (for map and reduce).
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        // Set the map and reduce classes.
        job.setMapperClass(InvertedIndexMapper.class);
        job.setReducerClass(InvertedIndexReducer.class);

        // Set the input and output file formats.
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        // Grab the input file and output directory from the command line.
        FileInputFormat.addInputPath(job, new Path(appArgs[0]));
        FileOutputFormat.setOutputPath(job, new Path(appArgs[1]));

        // Initiate the map-reduce job, and wait for completion.
        job.waitForCompletion(true);
    }
}
