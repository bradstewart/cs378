package com.refactorlabs.cs378.hw2;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import com.google.common.collect.Multiset;
import com.google.common.collect.HashMultiset;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.IOException;
import java.util.StringTokenizer;

/**
 * @author Brad Stewart. Created on 9/11/14.
 */
public class WordStatistics extends WordCount {

    /**
     * The Map class for word statistics.
     * Outputs a WordStatsWritable consisting of three LongWritables,
     *   [Document count, Word count, (Word count)^2]
     * @see org.apache.hadoop.mapreduce.Mapper
     */
    public static class WordStatsMapper extends Mapper<LongWritable, Text, Text, WordStatsWritable> {

        private Text word = new Text();

        /**
         *
         * @param key
         * @param value
         * @param context
         */

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            Multiset<String> words = HashMultiset.create();
            String line = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(line);

            while (tokenizer.hasMoreTokens()) {
                words.add(tokenizer.nextToken());
            }

            for (Multiset.Entry<String> entry : words.entrySet()) {
                word.set(entry.getElement());
                long count = entry.getCount();
                context.write(word, WordStatsWritable.build(new long[]{1L, count, (count * count)}));
            }

        }

    }

    public static class WordStatsCombiner extends Reducer<Text, WordStatsWritable, Text, WordStatsWritable> {

        @Override
        public void reduce(Text key, Iterable<WordStatsWritable> values, Context context) throws IOException, InterruptedException {
            context.write(key, WordStatsWritable.fromList(values));
        }
    }

    public static class WordStatsReducer extends Reducer<Text, WordStatsWritable, Text, Text> {
        @Override
        public void reduce(Text key, Iterable<WordStatsWritable> values, Context context)
                throws IOException, InterruptedException {

            WordStatsWritable ws = WordStatsWritable.fromList(values);
            Text result = new Text( ws.summary() );
            context.write(key, result);
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        String[] appArgs = new GenericOptionsParser(conf, args).getRemainingArgs();

        Job job = new Job(conf, "wordstats");
        // Identify the JAR file to replicate to all machines.
        job.setJarByClass(WordStatistics.class);

        // set map() output types
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(WordStatsWritable.class);

        // Set the output key and value types (for map and reduce).
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        // Set the map and reduce classes.
        job.setMapperClass(WordStatsMapper.class);
        job.setCombinerClass(WordStatsCombiner.class);
        job.setReducerClass(WordStatsReducer.class);

        // Set the input and output file formats.
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        // Grab the input file and output directory from the command line.
        FileInputFormat.addInputPath(job, new Path(appArgs[0]));
        FileOutputFormat.setOutputPath(job, new Path(appArgs[1]));

        // Initiate the map-reduce job, and wait for completion.
        job.waitForCompletion(true);
    }
}
