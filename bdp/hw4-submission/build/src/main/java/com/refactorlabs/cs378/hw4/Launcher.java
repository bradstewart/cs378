package com.refactorlabs.cs378.hw4;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.IOException;

/**
 * MapReduce job launcher to share common job configurations. Sets defaults
 *  based on the requirements on WordStatistics.
 *
 * @author Brad Stewart. Created on 9/26/14.
 */
public class Launcher {

    /*
     * Instance variables
     */
    protected String name;
    protected Job job;
    protected Configuration conf;

    /*
     * Constructors
     */
    public Launcher() throws IOException { }

    public Launcher(String name, Class jarClass) throws IOException{
        this.name = name;
        this.conf = new Configuration();
        this.job = new Job(conf, name);
        job.setJarByClass(jarClass);
        setDefaultClasses();
    }

    private void setDefaultClasses() {
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(WordStatisticsWritable.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(WordStatisticsWritable.class);
        job.setMapperClass(WordStatistics.MapClass.class);
        job.setReducerClass(WordStatistics.ReduceClass.class);
    }

    protected void setIOClasses(Class inputClass, Class outputClass) {
        // Set the input and output file formats.
        job.setInputFormatClass(inputClass);
        job.setOutputFormatClass(outputClass);
    }

    protected void setMapOutputClasses(Class keyClass, Class valueClass) {
        // set map() output types
        job.setMapOutputKeyClass(keyClass);
        job.setMapOutputValueClass(valueClass);
    }

    protected void setOutputClasses(Class keyClass, Class valueClass) {
        // set map() output types
        job.setOutputKeyClass(keyClass);
        job.setOutputValueClass(valueClass);
    }

    protected void setMapReduceClasses(Class mapClass, Class reduceClass) {
        // Set the map and reduce classes.
        job.setMapperClass(mapClass);
        job.setReducerClass(reduceClass);
    }

    protected void setMapReduceClasses(Class mapClass, Class reduceClass, Class combineClass) {
        job.setCombinerClass(combineClass);
        setMapReduceClasses(mapClass, reduceClass);
    }

    public void runJob(String[] args) throws Exception {

        String[] appArgs = new GenericOptionsParser(conf, args).getRemainingArgs();

        // Grab the input file and output directory from the command line.
        String[] inputPaths = appArgs[0].split(",");
        for ( String inputPath : inputPaths ) {
            FileInputFormat.addInputPath(job, new Path(inputPath));
        }

        FileOutputFormat.setOutputPath(job, new Path(appArgs[1]));

        // Initiate the map-reduce job, and wait for completion.
        job.waitForCompletion(true);
    }
}
