package com.refactorlabs.cs378.hw4;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Writable class containing the raw count data and calculated mean and variance.
 *
 * @author Brad Stewart. Created on 9/26/14.
 */
public class WordStatisticsWritable implements Writable {

    private static Joiner joiner = Joiner.on(",").skipNulls();
    private static Splitter splitter = Splitter.on(',').trimResults().omitEmptyStrings();

    /*
     * Instance variables for each recorded statistic
     */

    private LongWritable docCount;
    private LongWritable wordCount;
    private LongWritable sumOfSquares;
    private DoubleWritable mean;
    private DoubleWritable variance;

    /*
     * Constructors
     */

    public WordStatisticsWritable() {
        this(0, 0, 0);
    }

    public WordStatisticsWritable(long docCount, long wordCount, long sumOfSquares) {
        this.docCount = new LongWritable(docCount);
        this.wordCount = new LongWritable(wordCount);
        this.sumOfSquares = new LongWritable(sumOfSquares);
        this.mean = new DoubleWritable(mean());
        this.variance = new DoubleWritable(variance());
    }

    /**
     * Builds a new Writable by summing the counts of a list of Writable's.
     *
     * @param values
     * @return
     */
    public static WordStatisticsWritable build(Iterable<WordStatisticsWritable> values) {
        long docCount = 0, wordCount = 0, sumOfSquares = 0;

        for (WordStatisticsWritable w : values) {
            docCount += w.getDocCount();
            wordCount += w.getWordCount();
            sumOfSquares += w.getSumOfSquares();
        }
        return new WordStatisticsWritable(docCount, wordCount, sumOfSquares);
    }

    /**
     * Deserializes its string form (CSV) into a new Writable.
     *
     * @param value
     * @return
     */
    public static WordStatisticsWritable deserialize(String value) {
        String[] p = Iterables.toArray(splitter.split(value), String.class);
        long docCount = Long.parseLong(p[0]);
        long wordCount = Long.parseLong(p[1]);
        long sumOfSquares = Long.parseLong(p[2]);

        return new WordStatisticsWritable(docCount, wordCount, sumOfSquares);
    }


    @Override
    public void write(DataOutput out) throws IOException {
        docCount.write(out);
        wordCount.write(out);
        sumOfSquares.write(out);
        mean.write(out);
        variance.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        docCount.readFields(in);
        wordCount.readFields(in);
        sumOfSquares.readFields(in);
        mean.readFields(in);
        variance.readFields(in);
    }

    /**
     * Calculates the mean (wordCount/docCount).
     *
     * @return  mean
     */
    public double mean() {
        double wC = getWordCount();
        double dC = getDocCount();
        return (wC/dC);
    }

    /**
     * Calculates the variance.
     *
     * @return  variance
     */
    public double variance() {
        double wSq = getSumOfSquares();
        double dC = getDocCount();
        double mean = mean();
        double meanOfSq = (wSq/dC);
        double sqOfMean = mean*mean;
        return (meanOfSq - sqOfMean);
    }

    /*
     * Value getter methods.
     */

    public long getDocCount() {
        return docCount.get();
    }

    public long getWordCount() {
        return wordCount.get();
    }

    public long getSumOfSquares() {
        return sumOfSquares.get();
    }

    /**
     * Stringifies its values as a comma-separated string.
     *
     * @return CSV string
     */
    @Override
    public String toString() {
        return joiner.join(docCount, wordCount, sumOfSquares, mean, variance);
    }

    @Override
    public boolean equals(Object o) {
        WordStatisticsWritable w;

        if ( o instanceof WordStatisticsWritable)
            w = (WordStatisticsWritable) o;
        else
            return false;

        return w.docCount.equals(this.docCount) && w.wordCount.equals(this.wordCount) &&
                w.sumOfSquares.equals(this.sumOfSquares) && w.mean.equals(this.mean) && w.variance.equals(this.variance);
    }


}
