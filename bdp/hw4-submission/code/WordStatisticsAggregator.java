package com.refactorlabs.cs378.hw4;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.IOException;

/**
 * MapReduce class to aggregate statistics for a set of input files of the form
 *  "Word \t DocCount,WordCount,SumOfSquares,Mean,Variance. The input to it's
 *  mapper is expected to be the output of the WordStatistics job.
 *  Inherits and uses WordStatistics.ReduceClass.
 *
 * @author Brad Stewart. Created on 9/26/14.
 */
public class WordStatisticsAggregator extends WordStatistics {

    public static class MapClass extends Mapper<Text, Text, Text, WordStatisticsWritable> {

        /**
         *
         * @param key       a word
         * @param value     a CSV string -- DocCount,WordCount,SumOfSquares,Mean,Variance
         * @param context
         */

        @Override
        public void map(Text key, Text value, Context context) throws IOException, InterruptedException {

            context.write(key, WordStatisticsWritable.deserialize(value.toString()));
        }

    }

    public static void main(String[] args) throws Exception {
        Launcher launcher = new Launcher("WordStatisticsV2Aggregator", WordStatisticsAggregator.class);
        launcher.setIOClasses(KeyValueTextInputFormat.class, TextOutputFormat.class);
        launcher.setMapReduceClasses(WordStatisticsAggregator.MapClass.class, ReduceClass.class);
        launcher.runJob(args);
    }

}
