package com.refactorlabs.cs378;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;


/**
 * @author Brad Stewart. Created on 9/11/14.
 */
public class LongArrayWritable extends ArrayWritable {


    public LongArrayWritable() {
        super(LongWritable.class);
    }

    public LongArrayWritable(LongWritable[] values) {
        super(LongWritable.class, values);
    }

    public long[] getValueArray() {
        Writable[] wValues = get();
        long[] values = new long[wValues.length];

        for (int i = 0; i < values.length; i++) {
            values[i] = ((LongWritable)wValues[i]).get();
        }
        return values;
    }

    public LongArrayWritable setValueArray(long[] values) {
        LongWritable[] wValues = new LongWritable[values.length];

        for (int i = 0; i < values.length; i++) {
            wValues[i] = new LongWritable(values[i]);
        }
        this.set(wValues);
        return this;
    }

}
