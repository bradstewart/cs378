package com.refactorlabs.cs378.hw4;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * MapReduce class to calculate word statistics from a set of documents.
 *
 * @author Brad Stewart. Created on 9/26/14.
 */
public class WordStatistics {

    protected static Splitter splitter = Splitter.on(CharMatcher.BREAKING_WHITESPACE)
            .trimResults()
            .omitEmptyStrings();

    /**
     * Reads in a document as a string and outputs the DocCount, WordCount, and
     *  SumOfSquares for each token in the document.
     */
    public static class MapClass extends Mapper<LongWritable, Text, Text, WordStatisticsWritable> {

        private Text word = new Text();

        /**
         * @param key       File location
         * @param value     Document Text
         * @param context
         */
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            Multiset<String> words = HashMultiset.create();
            String line = value.toString();

            for (String token : splitter.split(line)) {
                words.add(token);
            }

            for (Multiset.Entry<String> entry : words.entrySet()) {
                word.set(entry.getElement());
                long count = entry.getCount();
                context.write(word, new WordStatisticsWritable(1L, count, (count * count)));
            }

        }

    }

    /**
     * Aggregates the DocCount, WordCount, and SumOfSquares over a list of Writables,
     *  and Calculates the mean and variance of the sums.
     *
     * Used as the Reducer and Combiner for this MapReduce job and the aggregating job.
     */
    public static class ReduceClass extends Reducer<Text, WordStatisticsWritable, Text, WordStatisticsWritable> {

        @Override
        public void reduce(Text key, Iterable<WordStatisticsWritable> values, Context context)
                throws IOException, InterruptedException {

            context.write(key, WordStatisticsWritable.build(values));
        }
    }

//    public static void main(String[] args) throws Exception {
//        Launcher launcher = new Launcher("WordStatisticsV2", WordStatistics.class);
//        launcher.runJob(args);
//    }
}
