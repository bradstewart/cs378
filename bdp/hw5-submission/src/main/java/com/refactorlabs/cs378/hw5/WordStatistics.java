package com.refactorlabs.cs378.hw5;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.refactorlabs.cs378.WordStatisticsData;
import org.apache.avro.Schema;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.avro.mapred.Pair;
import org.apache.avro.mapreduce.AvroJob;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * MapReduce class to calculate word statistics from a set of documents.
 *
 * Here the outputs (key and value) are both Avro objects and will be combined into
 * a LogEntryParser as the key, and null as the value.
 * written with output format: TextOutputFormat (creates an Avro container file).
 *
 * @author Brad Stewart. Created on 9/26/14.
 */
public class WordStatistics extends Configured implements Tool {

    protected static Splitter splitter = Splitter.on(CharMatcher.BREAKING_WHITESPACE)
            .trimResults()
            .omitEmptyStrings();

    /**
     * Reads in a document as a string and outputs the DocCount, WordCount, and
     *  SumOfSquares for each token in the document.
     */
    public static class MapClass extends Mapper<LongWritable, Text, Text, AvroValue<WordStatisticsData>> {

        private Text word = new Text();

        /**
         * @param key       File location
         * @param value     Document Text
         * @param context
         */
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            Multiset<String> words = HashMultiset.create();
            String line = value.toString();

            for (String token : splitter.split(line)) {
                words.add(token);
            }

            for (Multiset.Entry<String> entry : words.entrySet()) {
                word.set(entry.getElement());
                long wc = entry.getCount();

                WordStatisticsData.Builder builder = WordStatisticsData.newBuilder();
                builder.setDocCount(1L);
                builder.setWordCount(wc);
                builder.setSumOfSquares(wc*wc);
                context.write(word, new AvroValue(builder.build()));
            }

        }

    }

    /**
     * Aggregates the DocCount, WordCount, and SumOfSquares over a list of Avro data objects,
     *  and Calculates the mean and variance of the sums.
     *
     * Used as the Reducer and Combiner for this MapReduce job and the aggregating job.
     */
    public static class ReduceClass extends Reducer<Text, AvroValue<WordStatisticsData>, AvroKey<Pair<CharSequence, WordStatisticsData>>, NullWritable> {

        @Override
        public void reduce(Text key, Iterable<AvroValue<WordStatisticsData>> values, Context context)
                throws IOException, InterruptedException {

            WordStatisticsData.Builder builder = WordStatisticsData.newBuilder();
            long dc = 0, wc = 0, sq = 0;

            for (AvroValue<WordStatisticsData> w : values) {
                dc += w.datum().getDocCount();
                wc += w.datum().getWordCount();
                sq += w.datum().getSumOfSquares();
            }

            builder.setDocCount(dc);
            builder.setWordCount(wc);
            builder.setSumOfSquares(sq);
            builder.setMean(mean(wc, dc));
            builder.setVariance(variance(wc, dc, sq));

            context.write(
                    new AvroKey<Pair<CharSequence, WordStatisticsData>>
                            (new Pair<CharSequence, WordStatisticsData>(key.toString(), builder.build())),
                    NullWritable.get());
        }

        /**
         * Calculates the mean (wordCount/docCount).
         *
         * @return  mean
         */
        private double mean(long wc, long dc) {
            return (wc/dc);
        }

        /**
         * Calculates the variance.
         *
         * @return  variance
         */
        private double variance(long wc, long dc, double sq) {
            double mean = mean(wc, dc);
            double meanOfSq = (sq/dc);
            double sqOfMean = mean*mean;
            return (meanOfSq - sqOfMean);
        }
    }

    /**
     * The run() method is called (indirectly) from main(), and contains all the job
     * setup and configuration.
     */
    public int run(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.println("Usage: WordCountD <input path> <output path>");
            return -1;
        }

        Configuration conf = getConf();
        Job job = new Job(conf, "WordStatistics3");
        String[] appArgs = new GenericOptionsParser(conf, args).getRemainingArgs();

        // Identify the JAR file to replicate to all machines.
        job.setJarByClass(com.refactorlabs.cs378.hw5.WordStatistics.class);
        // Use this JAR first in the classpath (We also set a bootstrap script in AWS)
        conf.set("mapreduce.user.classpath.first", "true");

        // Specify the Map
        job.setInputFormatClass(TextInputFormat.class);
        job.setMapperClass(com.refactorlabs.cs378.hw5.WordStatistics.MapClass.class);
        job.setMapOutputKeyClass(Text.class);
        AvroJob.setMapOutputValueSchema(job, WordStatisticsData.getClassSchema());

        // Specify the Reduce
        job.setOutputFormatClass(TextOutputFormat.class);
        job.setReducerClass(com.refactorlabs.cs378.hw5.WordStatistics.ReduceClass.class);
        AvroJob.setOutputKeySchema(job, Pair.getPairSchema(Schema.create(Schema.Type.STRING), WordStatisticsData.getClassSchema()));
        job.setOutputValueClass(NullWritable.class);

        // Grab the input file and output directory from the command line.
        String[] inputPaths = appArgs[0].split(",");
        for ( String inputPath : inputPaths ) {
            FileInputFormat.addInputPath(job, new Path(inputPath));
        }
        FileOutputFormat.setOutputPath(job, new Path(appArgs[1]));

        // Initiate the map-reduce job, and wait for completion.
        job.waitForCompletion(true);

        return 0;
    }

    /**
     * Writes the classpath to standard out, for inspection.
     */
    public static void printClassPath() {
        ClassLoader cl = ClassLoader.getSystemClassLoader();
        URL[] urls = ((URLClassLoader) cl).getURLs();
        System.out.println("classpath BEGIN");
        for (URL url : urls) {
            System.out.println(url.getFile());
        }
        System.out.println("classpath END");
    }

    /**
     * The main method specifies the characteristics of the map-reduce job
     * by setting values on the Job object, and then initiates the map-reduce
     * job and waits for it to complete.
     */
    public static void main(String[] args) throws Exception {
        printClassPath();
        int res = ToolRunner.run(new Configuration(), new WordStatistics(), args);
        System.exit(res);
    }
}
