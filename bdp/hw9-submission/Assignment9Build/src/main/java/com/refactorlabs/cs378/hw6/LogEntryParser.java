package com.refactorlabs.cs378.hw6;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Creates a hash of key, value pairs parsed from the input log entry file.
 * Expects the following format: I \t |key:value| \t
 *
 * @author Brad Stewart. Created on 10/14/14.
 */
public class LogEntryParser {
    private static final char   ENTRY_START     = 'I';
    private static final String RECORD_DELIM    = "|\t|";
    private static final char   KEY_VALUE_DELIM = ':';
    // Take the first I\t off so we have a consistent format
    private static CharMatcher startMatcher = CharMatcher.is(ENTRY_START);
    // MapSplitter to split each part into key/value
    private static Splitter.MapSplitter splitter = Splitter.on(RECORD_DELIM)
                                                        .trimResults()
                                                        .omitEmptyStrings()
                                                        // Limit the secondary splitter to 2 parts (first :) since URIs
                                                        .withKeyValueSeparator(Splitter.on(KEY_VALUE_DELIM).limit(2));
    // Instance variable to store the key/values
    private Map<String, String> store;

    /**
     * Creates a new LogEntryParser with store initialized to the parameter
     *
     * @param values    initial HashMap
     */
    public LogEntryParser(Map<String, String> values) {
        store = values;
    }

    /**
     * Empty constructor.
     */
    public LogEntryParser() {
        store = new HashMap<String, String>();
    }

    /**
     * Constructor which populates the store hash from a string with the MapSplitter
     *
     * @param str   |key:value|\t|key2:value2| formatted string
     * @return
     */
    public static LogEntryParser parse(String str) {
        String items = startMatcher.trimLeadingFrom(str);
        return new LogEntryParser( splitter.split(items));
    }

    /**
     * Add an attribute pair to this store.
     *
     * @param k
     * @param v
     * @return  True if overwrote key, false otherwise
     */
    public boolean add(String k, String v) {
        return store.put(k, v) != null;
    }

    /**
     * Set of keys from the store.
     *
     * @return Set<String> of keys in the store
     */
    public Set<String> keys() {
        return store.keySet();
    }

    /**
     * Get a value from the store. If the key does not exist, the methods
     * returns the empty string rather than null.
     *
     * @param k
     * @return    String value or ""
     */
    public String get(String k) {
        return store.containsKey(k) ? store.get(k) : "";
    }

}
