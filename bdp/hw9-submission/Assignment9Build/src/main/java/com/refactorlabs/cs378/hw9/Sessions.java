package com.refactorlabs.cs378.hw9;

import com.refactorlabs.cs378.sessions.Session;
import org.apache.avro.mapreduce.AvroJob;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 *  Job class for combining Impression and Lead log entry files into Session objects.
 *
 * @author Brad Stewart. Created on 11/12/14.
 */
public class Sessions extends Configured implements Tool {

    /**
     * The run() method is called (indirectly) from main(), and contains all the job
     * setup and configuration.
     */
    public int run(String[] args) throws Exception {
        if (args.length != 4) {
            System.err.println("Usage: UserSessions <impression input path> <lead input path> <output path> <filter_percentage>");
            return -1;
        }

        Job job = new Job(getConf());
        job.setJobName("Sessions Filter");
        job.setJarByClass(Sessions.class);

        Configuration conf = job.getConfiguration();
        // Use this JAR first in the classpath (We also set a bootstrap script in AWS)
        conf.set("mapreduce.user.classpath.first", "true");

        String[] appArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        System.out.println("INPUT PERCENTAGE --- "+appArgs[3]);
        conf.set("filter_percentage", appArgs[3]);

        // DistributedCache
//        Path cacheFilePath = new Path(appArgs[3]);
//        DistributedCache.addCacheFile(cacheFilePath.toUri(), conf);

        // Specify the Map
        job.setMapOutputKeyClass(Text.class);
        AvroJob.setMapOutputValueSchema(job, Session.getClassSchema());

        // Specify the Reduce
        job.setReducerClass(com.refactorlabs.cs378.hw9.SessionReducer.class);

        // Grab the input file and output directory from the command line.
        MultipleInputs.addInputPath(job, new Path(appArgs[0]), TextInputFormat.class, ImpressionMapper.class);
        MultipleInputs.addInputPath(job, new Path(appArgs[1]), TextInputFormat.class, LeadMapper.class);


        //Configure outputs
        FileOutputFormat.setOutputPath(job, new Path(appArgs[2]));

        MultipleOutputs.addNamedOutput(job, "userType", TextOutputFormat.class, Text.class, Text.class);
        MultipleOutputs.setCountersEnabled(job, true);

        // Initiate the map-reduce job, and wait for completion.
        job.waitForCompletion(true);

        return 0;
    }

    /**
     * The main method specifies the characteristics of the map-reduce job
     * by setting values on the Job object, and then initiates the map-reduce
     * job and waits for it to complete.
     */
    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new Sessions(), args);
        System.exit(res);
    }
}

