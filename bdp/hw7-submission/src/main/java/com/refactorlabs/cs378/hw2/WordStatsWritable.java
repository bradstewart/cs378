package com.refactorlabs.cs378.hw2;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;


/**
 * Subclass of LongArrayWritable with methods specific to calculating
 *  the mean and variance for word occurrences.
 *
 * @author Brad Stewart. Created on 9/16/14.
 */
public class WordStatsWritable extends LongArrayWritable {

    /**
     * Computes an array of sums for the value arrays of the Iterable objects.
     *
     * @param values
     * @return
     */
    public static long[] combineValues( Iterable<WordStatsWritable> values ) {
        long[] sums = new long[]{ 0L, 0L, 0L };

        for (LongArrayWritable value : values) {
            long[] lValues = value.getValueArray();
            for (int i = 0; i < lValues.length; i++) {
                sums[i] += lValues[i];
            }
        }
        return sums;
    }

    /**
     * Creates a new Writable which holds the summed values of each Writable in the list.
     *
     * @param values
     * @return
     */
    public static WordStatsWritable fromList( Iterable<WordStatsWritable> values ) {
        long[] sums = combineValues(values);
        return build(sums);
    }

    /**
     * Creates a new Writable from a raw long array.
     *
     * @param lValues
     * @return
     */
    public static WordStatsWritable build(long[] lValues) {
        WordStatsWritable ws = (WordStatsWritable) new WordStatsWritable().setValueArray(lValues);
        return ws;
    }

    /**
     * Summarizes this Writable's data and formats the output.
     *
     * @return
     */
    public String summary() {
        String output = ", " + getDocCount() + ", " + fmt(mean()) + ", " + fmt(variance());
        return output;
    }

    public double mean() {
        double wC = getWordCount();
        double dC = getDocCount();
        return (wC/dC);
    }

    public double variance() {
        double wSq = getWordCountSq();
        double dC = getDocCount();
        double mean = mean();
        double meanOfSq = (wSq/dC);
        double sqOfMean = mean*mean;
        return (meanOfSq - sqOfMean);
    }

    public long getDocCount() {
        return getLong(0);
    }

    public long getWordCount() {
        return getLong(1);
    }

    public long getWordCountSq() {
        return getLong(2);
    }

    private long getLong(int index) {
        Writable[] wValues = get();
        return ((LongWritable)wValues[index]).get();
    }

    private String fmt(double num) {
        return String.format("%.2f", num);
    }


}
