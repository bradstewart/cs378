package com.refactorlabs.cs378.hw7;

import com.google.common.base.Strings;
import com.google.common.collect.*;
import com.refactorlabs.cs378.sessions.Impression;
import com.refactorlabs.cs378.sessions.ImpressionType;
import com.refactorlabs.cs378.sessions.Lead;
import com.refactorlabs.cs378.sessions.Session;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.avro.mapred.Pair;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Reduce class combining the outputs from the ImpressionMapper and LeadMapper into a single Session object.
 * An individual Lead's vdp_index is set to the index of the matching Impression in the Impression array,
 * where matching is determined by Id. If no match is found, it defaults to -1.
 *
 * @author Brad Stewart. Created on 10/28/14.
 */
public class SessionReducer extends Reducer<Text, AvroValue<Session>, AvroKey<Pair<CharSequence, Session>>, NullWritable> {

    /**
     *
     * @param key           Session.user_id:Session.api_key
     * @param values        Avro Session object list
     * @param context
     * @throws java.io.IOException
     * @throws InterruptedException
     */
    @Override
    public void reduce(Text key, Iterable<AvroValue<Session>> values, Context context)
            throws IOException, InterruptedException {

        // TreeSet to sort the combined list
        Set<Impression> impressions = Sets.newTreeSet(new ImpressionTimestampComparator());
        Set<Lead> leads = Sets.newTreeSet();

        Session session = null;
        boolean isImpression = false;

        for( AvroValue<Session> value : values) {
            Session s = value.datum();

            // We only need the session data from one object
            if (session == null) {
                session = Session.newBuilder(value.datum()).build();
            }

            // But the Impression related Sessions have a lot more data, so if we have
            // one of those, use it for the session.
            if (!isImpression && s.getImpressions().size() > 0) {
                isImpression = true;
                session = Session.newBuilder(value.datum()).build();
            }
            //Defensive copy this Session's impression and lead lists into the combined set
            impressions.addAll(ImmutableList.copyOf(value.datum().getImpressions()));
            leads.addAll(ImmutableList.copyOf(value.datum().getLeads()));
        }

        // Map impression ids to its index
        Map<Long, Integer> impressionIds = mapImpressionIds(impressions);

        // Assign vdp_index values based on the map
        for (Lead lead : leads) {
            Long id = lead.getId();
            Integer index = -1;

            if (impressionIds.containsKey(id))
                index = impressionIds.get(id);

            lead.setVdpIndex(index);
        }

        session.setImpressions(Lists.newArrayList(impressions));
        session.setLeads(Lists.newArrayList(leads));

        context.write(
                new AvroKey<Pair<CharSequence, Session>>
                        (new Pair<CharSequence, Session>(key.toString(), session)),
                NullWritable.get());

    }

    /**
     * Maps a Set of Impressions to a Hash with the keys set to its Id and value its index in the
     *  Set/Array. Only VDP Impressions occuring before a THANK_YOU are are added to the map.
     *
     * @param impressions   Set of impressions
     *
     * @return  Hash<Id, Index>
     */
    protected Map<Long, Integer> mapImpressionIds( Set<Impression> impressions ) {
        Map<Long, Integer> impressionIds = Maps.newHashMap();

        int i = 0;
        for (Impression impression : impressions) {

            if (impression.getImpressionType().equals(ImpressionType.VDP)) {
                for(Long id : impression.getId()) {
                    impressionIds.put(id, i);
                }
            } else if (impression.getImpressionType().equals(ImpressionType.THANK_YOU)) {
                break;
            }
            i++;
        }

        return impressionIds;
    }
}
