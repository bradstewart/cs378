package com.refactorlabs.cs378.hw7;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.Set;

/**
 * Creates a hash of key, value pairs parsed from the input log entry file.
 * Expects the following format: [I,L] \t |key:value| \t
 *
 * @author Brad Stewart. Created on 10/14/14.
 */
public class LogParser {
    public static final String LOG_ENTRY_TYPE = "log entry type";
    private static final String EMPTY_STRING = "";

    // Instance variable to store the key/values
    private Map<String, String> store;

    /**
     * Creates a new LogEntryParser with store initialized to the parameter
     *
     * @param values    initial HashMap
     */
    public LogParser(Map<String, String> values) {
        store = values;
    }

    /**
     * Empty constructor.
     */
    public LogParser() {
        store = Maps.newHashMapWithExpectedSize(50);
    }

    /**
     * Constructor which populates the store hash from a string with the MapSplitter
     *
     * @param logEntry   |key:value|\t|key2:value2| formatted string
     * @return
     */
    public static LogParser parse(String logEntry) {
        Map<String, String> keyValueMap = Maps.newHashMapWithExpectedSize(50);

        // Log file starts with a string followed by a tab.
        // This first string indicates the log entry type.
        String[] fields = logEntry.split("\t", 2);
        if (fields.length > 0) {
            keyValueMap.put(LOG_ENTRY_TYPE, fields[0]);
        }

        // The remainder of the log entry is parameter/value pairs (parameter and value
        // are separated by a colon), and parameter/value pairs are separated by "|\t|".
        if (fields.length > 1) {
            fields = fields[1].split("\\|\t\\|");

            for ( String field : fields ) {
                String[] pvPair = field.split(":", 2);

                if (pvPair.length == 2) {
                    keyValueMap.put(pvPair[0], pvPair[1]);
                } else if (pvPair.length == 1) {
                    keyValueMap.put(pvPair[0], EMPTY_STRING);
                }
            }
        }

        return new LogParser( keyValueMap );
    }

    /**
     * Add an attribute pair to this store.
     *
     * @param k
     * @param v
     * @return  True if overwrote key, false otherwise
     */
    public boolean add(String k, String v) {
        return store.put(k, v) != null;
    }

    /**
     * Set of keys from the store.
     *
     * @return Set<String> of keys in the store
     */
    public Set<String> keys() {
        return store.keySet();
    }

    /**
     * Get a value from the store. If the key does not exist, the methods
     * returns the empty string rather than null.
     *
     * @param k
     * @return    String value or ""
     */
    public String get(String k) {
        return store.containsKey(k) ? store.get(k) : "";
    }

}
