package com.refactorlabs.cs378.hw7;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Floats;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.refactorlabs.cs378.sessions.*;


/**
 * Util methods to convert parsed text values into Avro Session object appropriate values.
 *
 * @author Brad Stewart. Created on 10/14/14.
 */
public class Utils {

    /*
     *   Enumerator helpers return the the Enum which matches the input string if it exists or
     *   the default Enum value.
     */

    public static ActiveX getActiveX(String value) {
        for (ActiveX item : ActiveX.values()) {
            if (item.name().equalsIgnoreCase(value))
                return item;
        }
        return ActiveX.NOT_SUPPORTED;
    }

    public static Action getAction(String value) {
        for (Action item : Action.values()) {
            if (item.name().equalsIgnoreCase(value))
                return item;
        }
        return Action.PAGE_VIEW;
    }

    public static ActionName getActionName(String value) {
        for (ActionName item : ActionName.values()) {
            if (item.name().equalsIgnoreCase(value))
                return item;
        }
        return ActionName.UNKNOWN;
    }

    // Maps action:values from the input to the appropriate Enum type
    private static final ImmutableMap<String, ImpressionType> IMPRESSION_TYPE_MAP = ImmutableMap.of(
            "email",   ImpressionType.VDP,
            "phone",   ImpressionType.VDP,
            "landing", ImpressionType.VDP,
            "thankyou", ImpressionType.THANK_YOU,
            "action",  ImpressionType.ACTION
    );

    public static ImpressionType getImpressionType(String value) {
        value = value.toLowerCase();
        if (IMPRESSION_TYPE_MAP.containsKey(value))
            return IMPRESSION_TYPE_MAP.get(value);

        for (ImpressionType item : ImpressionType.values()) {
            if (item.name().equalsIgnoreCase(value))
                return item;
        }
        return ImpressionType.SRP;
    }

    public static Vertical getVertical(String value) {
        for (Vertical item : Vertical.values()) {
            if (item.name().equalsIgnoreCase(value))
                return item;
        }
        return Vertical.OTHER;
    }

    public static PhoneType getPhoneType(String value) {
        for (PhoneType item : PhoneType.values()) {
            if (item.name().equalsIgnoreCase(value))
                return item;
        }
        return PhoneType.NONE;
    }

    public static BidType getBidType(String value) {
        for (BidType item : BidType.values()) {
            if (item.name().equalsIgnoreCase(value))
                return item;
        }

        if(!Strings.isNullOrEmpty(value))
            return BidType.OTHER;

        return BidType.LEAD;
    }

    public static LeadType getLeadType(String value) {
        for (LeadType item : LeadType.values()) {
            if (item.name().equalsIgnoreCase(value))
                return item;
        }
        return LeadType.BAD;
    }

    /*
     *   Numeric helpers return the Numeric object created from parsing the string.
     *   If the string cannot be parsed, it returns the default value.
     */

    private static final Double DEFAULT_DOUBLE = 0.0;

    public static Double getDouble(String value) {
        Double result = Doubles.tryParse(value);
        if (result == null)
            result = DEFAULT_DOUBLE;
        return result;
    }

    private static final Long DEFAULT_LONG = 0L;

    public static Long getLong(String value) {
        Long result = Longs.tryParse(value);
        if (result == null)
            result = DEFAULT_LONG;
        return result;
    }

    private static final Integer DEFAULT_INT = 0;

    public static Integer getInt(String value) {
        Integer result = Ints.tryParse(value);
        if (result == null)
            result = DEFAULT_INT;
        return result;
    }

    private static final Float DEFAULT_FLOAT = 0f;

    public static Float getFloat(String value) {
        Float result = Floats.tryParse(value);
        if (result == null)
            result = DEFAULT_FLOAT;
        return result;
    }

    public static Boolean getBool(String value) {
        Boolean result = Boolean.parseBoolean(value);
        if (result == null)
            result = false;
        return result;
    }
}
