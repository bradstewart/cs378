package com.refactorlabs.cs378.hw11;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.refactorlabs.cs378.hw4.WordStatistics;
import com.refactorlabs.cs378.hw4.WordStatisticsWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author Brad Stewart. Created on 12/2/14.
 */
public class WordStatsWithLength extends WordStatistics {

    /**
     * Reads in a document as a string and outputs the DocCount, WordCount, and
     *  SumOfSquares for each token in the document. Additionally outputs a "Length" field.
     */
    public static class MapClassWithLength extends WordStatistics.MapClass {

        protected static String MESSAGE_LENGTH = "---MESSAGE LENGTH---";

        /**
         * @param key       File location
         * @param value     Document Text
         * @param context
         */
        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            Multiset<String> words = HashMultiset.create();
            String line = value.toString();

            for (String token : splitter.split(line)) {
                words.add(token);
            }

            // Output an extra field corresponding to the length of the message
            word.set(MESSAGE_LENGTH);
            long count = words.size();
            context.write(word, new WordStatisticsWritable(1L, count, (count * count)));

            // Output the statistics for each word
            for (Multiset.Entry<String> entry : words.entrySet()) {
                word.set(entry.getElement());
                count = entry.getCount();
                context.write(word, new WordStatisticsWritable(1L, count, (count * count)));
            }

        }

    }


}
