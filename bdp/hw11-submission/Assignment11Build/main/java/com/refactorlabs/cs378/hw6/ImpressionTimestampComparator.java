package com.refactorlabs.cs378.hw6;

import com.refactorlabs.cs378.sessions.Impression;

import java.util.Comparator;

/**
 * Comparator to sort a Collection of Impressions by timestamp.
 *
 * @author Brad Stewart. Created on 10/14/14.
 */
public class ImpressionTimestampComparator implements Comparator<Impression> {

    @Override
    public int compare(Impression im1, Impression im2) {
        return im1.getTimestamp().compareTo(im2.getTimestamp());
    }
}
