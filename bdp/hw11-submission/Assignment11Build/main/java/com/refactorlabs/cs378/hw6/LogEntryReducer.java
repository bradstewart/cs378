package com.refactorlabs.cs378.hw6;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.refactorlabs.cs378.sessions.Impression;
import com.refactorlabs.cs378.sessions.Session;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.avro.mapred.Pair;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Set;

/**
 * Reducer for the UserSession MR job. Combines the Impression list's for a set of equal Sessions.
 *
 * @author Brad Stewart. Created on 10/14/14.
 */
public class LogEntryReducer extends Reducer<Text, AvroValue<Session>, AvroKey<Pair<CharSequence, Session>>, NullWritable> {

    /**
     *
     * @param key           Session.user_id:Session.api_key
     * @param values        Avro Session object list
     * @param context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    public void reduce(Text key, Iterable<AvroValue<Session>> values, Context context)
            throws IOException, InterruptedException {

        // TreeSet to sort the combined list
        Set<Impression> impressions = Sets.newTreeSet( new ImpressionTimestampComparator() );

        Session session = null;
        for( AvroValue<Session> value : values) {
            // We only need the session data from one object
            if (session == null) {
                session = Session.newBuilder(value.datum()).build();
            }
            //Defensive copy this Session's impression list and add it to the set
            ImmutableList<Impression> defCopy = ImmutableList.copyOf(value.datum().getImpressions());
            impressions.addAll(defCopy);
        }

        session.setImpressions(Lists.newArrayList(impressions));
        context.write(
                new AvroKey<Pair<CharSequence, Session>>
                        (new Pair<CharSequence, Session>(key.toString(), session)),
                NullWritable.get());

    }
}
