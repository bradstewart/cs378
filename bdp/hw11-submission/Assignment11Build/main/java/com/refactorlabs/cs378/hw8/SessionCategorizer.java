package com.refactorlabs.cs378.hw8;

import com.refactorlabs.cs378.sessions.Impression;
import com.refactorlabs.cs378.sessions.ImpressionType;
import com.refactorlabs.cs378.sessions.Session;

/**
 * Categorizes Sessions based on the criteria provide in HW8.
 *
 * @author Brad Stewart. Created on 11/4/14.
 */
public class SessionCategorizer {

    private Session session;

    public SessionCategorizer(Session session) {
        this.session = session;
    }

    /**
     * Static convenience method to instantiate a SessionCategorizer and categorize the session.
     *
     * @param session
     * @return
     */
    public static String categorize(Session session) {
        SessionCategorizer c = new SessionCategorizer(session);
        return c.categorize();
    }

    /**
     * Categorize a provided Session.
     *
     * @return  String name of the category
     */
    private String categorize() {
        String result = "";

        if (hasLeads())
            result = "submitter";
        else if (hasOneImpression())
            result = "bouncer";
        else if (hasOnlySrpImpressions())
            result = "browser";
        else
            result = "searcher";

        return result;
    }

    /*
     * Private condition methods.
     */

    private boolean hasLeads() {
        return (session.getLeads().size() > 0);
    }

    private boolean hasOneImpression() {
        return (session.getImpressions().size() == 1);
    }

    private boolean hasOnlySrpImpressions() {
        if (session.getImpressions().size() == 0) {
            return false;
        }

        for ( Impression impression : session.getImpressions() ) {
            if (!impression.getImpressionType().equals(ImpressionType.SRP)) {
                return false;
            }
        }

        return true;
    }
}
