package com.refactorlabs.cs378.hw7;

import com.google.common.collect.Lists;
import com.refactorlabs.cs378.sessions.Impression;
import com.refactorlabs.cs378.sessions.Lead;

import java.util.List;

/**
 * Maps LogEntry's beginning with "L" as defined by the MultipleInputs for this job. The majority
 * of this class is inherited from the ImpressionMapper.
 *
 * @author Brad Stewart. Created on 10/28/14.
 */
public class LeadMapper extends ImpressionMapper {

    /**
     * Lead logs define this id under a different name, so override it.
     *
     * @return  "user_id" pared from the log
     */
    @Override
    protected String userId() {
        return get("userid");
    }

    /**
     * Build a Lead for this log entry and ad it as the only element to a list.
     *
     * @return  List containing one built Lead object
     */
    @Override
    protected List<Lead> buildLeadList() {
        Lead.Builder builder = Lead.newBuilder();
        builder.setAb(get("ab"))
                .setAdvertiser(get("advertiser"))
                .setAmount(Utils.getFloat(get("lead_amount")))
                .setBidType(Utils.getBidType(get("bidtype")))
                .setCampaignId(get("campaign_id"))
                .setId(Utils.getLong(get("recordid")))
                .setLeadId(Utils.getLong(get("lead_id")))
                .setRevenue(Utils.getFloat(get("revenue")))
                .setTest(Utils.getBool(get("test")))
                .setType(Utils.getLeadType(get("type")));

        return Lists.newArrayList(builder.build());
    }

    /**
     * Build an empty Impression list.
     *
     * @return  An empty Impression list.
     */
    @Override
    protected List<Impression> buildImpressionList() {
        return Lists.newArrayList();
    }

}
