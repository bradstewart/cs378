package com.refactorlabs.cs378.hw9;

import com.refactorlabs.cs378.sessions.ActionName;
import com.refactorlabs.cs378.sessions.Impression;
import com.refactorlabs.cs378.sessions.Session;

import java.util.Random;


/**
 * Filters the outputs Reduce-side with specific criteria for each type of session.
 *
 * @author Brad Stewart. Created on 11/12/14.
 */
public class SessionFilter {

    private Double nPercent;
    private Double mPercent;
    private Random rand = new Random();

    public SessionFilter(Double percent) {
        nPercent = (Double)(percent/100.0);
        mPercent = (Double)((percent/10.0)/100.0);
    }

    /**
     *
     * @param session
     * @param category
     * @return True if this record should be included in the outputs, False otherwise.
     */
    public boolean include(Session session, String category) {
        if(category.equals("submitter"))
            return includeSubmitter(session);
        else if(category.equals("searcher"))
            return includeSearcher(session);
        else if(category.equals("browser"))
            return includeBrowser(session);
        else if(category.equals("bouncer"))
            return includeBouncer(session);
        else
            return false;
    }

    public boolean includeSearcher(Session session) {
        for (Impression imp : session.getImpressions()) {
            ActionName a = imp.getActionName();

            if (a.equals(ActionName.VIEWED_CARFAX_REPORT)
                    || a.equals(ActionName.VIEWED_CARFAX_REPORT_UNHOSTED)) {
                if(rand.nextDouble() < nPercent) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean includeBrowser(Session session) {
        return (rand.nextDouble() < nPercent);
    }

    public boolean includeBouncer(Session session) {
        return (rand.nextDouble() < mPercent);
    }

    public boolean includeSubmitter(Session session) {
        return true;
    }

}
