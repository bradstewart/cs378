package com.refactorlabs.cs378.hw11;

import com.refactorlabs.cs378.hw4.WordStatistics;
import com.refactorlabs.cs378.hw4.WordStatisticsWritable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * @author Brad Stewart. Created on 12/2/14.
 */
public class RandomInputs extends Configured implements Tool {

    /**
     * The run() method is called (indirectly) from main(), and contains all the job
     * setup and configuration.
     */
    public int run(String[] args) throws Exception {
        /* Configure job */
        Job job = new Job(getConf());
        job.setJobName("Random Inputs");
        job.setJarByClass(RandomInputs.class);

        /* Process command line arguments */
        Configuration conf = job.getConfiguration();
        // Use this JAR first in the classpath (We also set a bootstrap script in AWS)
        conf.set("mapreduce.user.classpath.first", "true");

        if (args.length != 3) {
            System.err.println("Usage: RandomInputs <output path> <num_map_tasks> <num_records_per_map_task");
            return -1;
        }
        String[] appArgs = new GenericOptionsParser(conf, args).getRemainingArgs();

        /* Configure IO */
        FileOutputFormat.setOutputPath(job, new Path(appArgs[0]));

        job.setInputFormatClass(RandomInputFormat.class);
        RandomInputFormat.setNumMapTasks(job, Integer.parseInt(appArgs[1]));
        RandomInputFormat.setNumRecordsPerTask(job, Integer.parseInt(appArgs[2]));


        /* Configure Mappers and Reducers */
        // Specify the Map
        job.setMapperClass(WordStatsWithLength.MapClassWithLength.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(WordStatisticsWritable.class);

        //Specify the Combine
        job.setCombinerClass(WordStatistics.ReduceClass.class);

        // Specify the Reduce
        job.setReducerClass(WordStatistics.ReduceClass.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(WordStatisticsWritable.class);

        /* Initiate job */
        job.waitForCompletion(true);
        return 0;
    }

    /**
     * The main method specifies the characteristics of the map-reduce job
     * by setting values on the Job object, and then initiates the map-reduce
     * job and waits for it to complete.
     */
    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new RandomInputs(), args);
        System.exit(res);
    }
}