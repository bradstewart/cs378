package com.refactorlabs.cs378.hw11;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.IOException;
import java.util.Random;

/**
 * RecordReader used by RandomInputFormat to actually generate the random messages.
 *
 * @author Brad Stewart. Created on 12/2/14.
 */
public class RandomRecordReader extends RecordReader<LongWritable, Text> {

    private static String[] wordList = new String[] {"The", "quick", "brown", "fox", "jumped", "over", "the", "lazy", "dog", "sir"};

    private int numRecordsToCreate = 0;
    private int createdRecords = 0;

    private int standardDeviation = 10;
    private int meanLength = 50;

    private LongWritable key = new LongWritable();
    private Text value = new Text();

    private Random random = new Random();


    @Override
    public void initialize(InputSplit inputSplit, TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {
        this.numRecordsToCreate = taskAttemptContext.getConfiguration().getInt(RandomInputFormat.NUM_RECORDS_PER_TASK, -1);
    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {
        // Don't exceed the specified number of records
        if ( createdRecords < numRecordsToCreate ) {
            key.set(createdRecords++);
            value.set(getRandomText());
            return true;
        }

        return false;
    }

    /**
     * Selects a random number of random words from the provided list and concatenates them
     * into a single string suitable for an ouptut message.
     *
     * @return String message
     */
    private String getRandomText() {
        // Normal dist. offset by the desired mean and scaled by the desired Std. Dev.
        Double dblLength = standardDeviation * random.nextGaussian() + meanLength;
        int length = dblLength.intValue();

        StringBuilder str = new StringBuilder();

        for (int i = 0; i < length; i++) {
            // Get a random word from the list and append it
            str.append(wordList[Math.abs(random.nextInt() % wordList.length)] + " ");
        }

        return str.toString();
    }

    @Override
    public LongWritable getCurrentKey() throws IOException, InterruptedException {
        return key;
    }

    @Override
    public Text getCurrentValue() throws IOException, InterruptedException {
        return value;
    }

    @Override
    public float getProgress() throws IOException, InterruptedException {
        return (float) createdRecords / (float) numRecordsToCreate;
    }

    @Override
    public void close() throws IOException {
        // Nothing to do.
    }
}
