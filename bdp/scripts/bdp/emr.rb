
require_relative 'aws_client'

require 'aws-sdk'
require 'waitutil'

class EMRClient < AWSClient

  ACTIVE_STATES = ['RUNNING', 'WAITING']

  def initialize
    super
    @emr = AWS::EMR.new
    @client = @emr.client
  end

  def job_flows
    @emr.job_flows
  end

  def active_job_flow
    job_flows.with_state('RUNNING', 'WAITING').detect { |j| j.name == 'bjs2639' }
  end

  def run_job(options = {})
    job = active_job_flow || launch_cluster
    puts job.inspect
    step = build_step(options)
    puts step
    job.add_steps(step)
    # @client.run_job_flow(job.as_hash)
    # puts job.inspect
  end

  private

  def launch_cluster
    job_flow = @emr.job_flows.create('bjs2639', default_job_flow)
    puts "Starting new cluster..."
    WaitUtil.wait_for_condition("cluster to initialize", verbose: true, timeout_sec: 180, delay_sec: 10) do
      ACTIVE_STATES.include? job_flow.state
    end
    job_flow
  end

  def default_job_flow
    # THIS NEEDS MORE ATTRIBUTS LIKE KEY NAME AND SOFTWARE VERSION TO WORK
    {
      name: 'bjs2639',
      log_uri: 's3://utcs378/bjs2639/logs/',
      :ami_version => '2.4.7',
      :instances => {
        :instance_count => 2,
        :master_instance_type => 'm1.small',
        :slave_instance_type => 'm1.small',
        :ec2_key_name => 'bdp',
        :termination_protected => false,
        :keep_job_flow_alive_when_no_steps => true,
        :hadoop_version => '1.0.3'
      },
      :steps => [],
      :visible_to_all_users => false
    }
  end

  def build_step(options = {})
    step = default_step
    step[:hadoop_jar_step][:jar] = jar_url(options[:jar_name])
    step[:hadoop_jar_step][:args] = options[:jar_args]
    [step]
  end

  def default_step
    {
      :name => 'default_jar_step',
      :action_on_failure => 'CONTINUE',
      :hadoop_jar_step => {}
    }
  end

  def add_step(jar_name, options = {})
    step = {
      :name => options[:step_name] || 'default_jar_step',
      :action_on_failure => 'CONTINUE',
      :hadoop_jar_step => {
        :jar => jar_url(jar_name),
        :args => options[:jar_args]
      }
    }
    (@job[:steps] || []) << step
  end

  def jar_url(name)
    "s3://utcs378/bjs2639/jars/#{name}"
  end

  def input_url(name)
    "s3n://utcs378/data/#{name}.txt"
  end

  def output_url(name)
    "s3n://utcs378/bjs2639/output/#{name}"
  end

end

