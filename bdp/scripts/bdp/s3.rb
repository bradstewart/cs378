require_relative 'aws_client'

require 'aws-sdk'
require 'uuid'
require 'waitutil'

class S3Client < AWSClient

  def initialize
    super
    @s3 = AWS::S3.new
  end

  def bucket
    @s3.buckets['utcs378']
  end

  def jars
    objects('jars')
  end

  def output(name)
    objects("output/#{name}")
  end

  def upload_jar(file)
    # path = File.expand_path('../cs378/out/artifacts/cs378_jar/cs378.jar')
    File.open(file) do |file|
      name = File.basename(file)
      get_jar(name).write(file)
    end
  end

  def clean_output(dataset)
    new_prefix = "bjs2639/previous_outputs/#{dataset}/#{unique_id}"
    output(dataset).each do |obj|
      name = file_name_for(obj.key)
      obj.move_to("#{new_prefix}/#{name}") unless name == dataset
    end
  end

  private

  def previous_output_path(dataset)
    prefix("output/#{dataset}/#{unique_id}")
  end

  def file_name_for(key)
    key.split('/')[-1]
  end

  def unique_id
    UUID.new.generate
  end

  def get_jar(name)
    bucket.objects["#{prefix('jars')}/#{name}"]
  end

  def objects(prefix)
    bucket.objects.with_prefix(prefix(prefix))
  end

  def prefix(prefix)
    "bjs2639/#{prefix}"
  end

end

