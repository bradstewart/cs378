#! /usr/bin/env ruby

require 'rubygems'
require 'bundler/setup'
require 'nokogiri'

require_relative 'bdp/client'


system( "mvn clean package" )

Dir.glob("#{Dir.pwd}/target/bdp-*.jar") { |file|
  @jar = File.new(file)
}

abort("Maven buld failed") if @jar.nil?

client = BDP::Client.new
# client.running_clusters
client.deploy(@jar, jar_args: ['s3n://utcs378/data/dataSet1.txt', 's3n://utcs378/bjs2639/output/dataSet1'])

# pom = Nokogiri::XML(File.open("#{Dir.pwd}/pom.xml"))
# name = pom.at_css("project artifactId").content
# name = pom.at_css("project artifactId").content
# name = pom.at_css("project artifactId").content

