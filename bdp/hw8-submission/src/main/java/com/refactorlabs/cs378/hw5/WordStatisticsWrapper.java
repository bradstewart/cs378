package com.refactorlabs.cs378.hw5;

import com.refactorlabs.cs378.WordStatisticsData;
import org.apache.avro.mapred.AvroValue;

/**
 * Wrapper to handle some of the value setting and common calculations.
 * @author Brad Stewart. Created on 10/7/14.
 */
public class WordStatisticsWrapper {

    public static AvroValue<WordStatisticsData> build(long dc, long wc, long sq) {
        WordStatisticsData.Builder builder = WordStatisticsData.newBuilder();
        builder.setDocCount(dc);
        builder.setWordCount(wc);
        builder.setSumOfSquares(sq);
        return new AvroValue(builder.build());
    }

    /**
     * NOTE: This method is NOT used as I ran into 'Could not infer Schema' errors from Avro and did not
     *  figure out the root cause.
     * @param values
     * @return
     */
    public static AvroValue<WordStatisticsData> build(Iterable<AvroValue<WordStatisticsData>> values) {
        long dc = 0, wc = 0, sq = 0;

        for (AvroValue<WordStatisticsData> w : values) {
            dc += w.datum().getDocCount();
            wc += w.datum().getWordCount();
            sq += w.datum().getSumOfSquares();
        }

        WordStatisticsData.Builder builder = WordStatisticsData.newBuilder();
        builder.setDocCount(dc);
        builder.setWordCount(wc);
        builder.setSumOfSquares(sq);
        builder.setMean(mean(wc, dc));
        builder.setVariance(variance(wc, dc, sq));
        return new AvroValue(builder.build());
    }

    /**
     * Calculates the mean (wordCount/docCount).
     *
     * @return  mean
     */
    private static double mean(long wc, long dc) {
        return (wc/dc);
    }

    /**
     * Calculates the variance.
     *
     * @return  variance
     */
    private static double variance(long wc, long dc, double sq) {
        double mean = mean(wc, dc);
        double meanOfSq = (sq/dc);
        double sqOfMean = mean*mean;
        return (meanOfSq - sqOfMean);
    }

}
