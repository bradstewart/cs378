package com.refactorlabs.cs378.hw8;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.refactorlabs.cs378.hw7.LogParser;
import com.refactorlabs.cs378.hw7.Utils;
import com.refactorlabs.cs378.sessions.Impression;
import com.refactorlabs.cs378.sessions.Lead;
import com.refactorlabs.cs378.sessions.Session;
import org.apache.avro.mapred.AvroValue;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Maps LogEntry's beginning with "I" as defined by the MultipleInputs for this job.
 *
 * @author Brad Stewart. Created on 11/4/14.
 */
public class ImpressionMapper extends Mapper<LongWritable, Text, Text, AvroValue<Session>> {

    protected static Splitter s = Splitter.on(',');
    protected Text sessionId = new Text();
    protected LogParser parser;
    protected Map<String, String> zipToDma;

    /**
     * Process the replicated join file specified by the Configuration object from the DistributedCache
     * into a HashMap<Zip, Dma>.
     *
     * @param context
     * @throws IllegalArgumentException
     * @throws IOException
     */
    @Override
    protected void setup(Context context) throws InterruptedException, IOException {
        zipToDma = Maps.newHashMap();

        Path[] paths = DistributedCache.getLocalCacheFiles(context.getConfiguration());

        for (Path path : paths) {
            Scanner scanner = new Scanner(new File(path.toString()));

            while(scanner.hasNext()) {
                String line = scanner.nextLine();
                String[] parts = line.split(",");
                zipToDma.put(parts[0], parts[1]);
            }
        }
    }

    /**
     * @param key       File location
     * @param value     Document Text
     * @param context
     */
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        // Parse the string into a hash
        parser = LogParser.parse(value.toString());
        sessionId.set(sessionId());
        context.write(sessionId, new AvroValue<Session>(buildSession()));
    }

    /**
     * Constructs the sessionId from the uid/user_id and apikey/api_key fields.
     *
     * @return      SessionId string
     */
    protected String sessionId() {
        return userId() + ":" + get("apikey");
    }

    protected String userId() {
        return get("uid");
    }

    /**
     * Tries multiple possible keys to get the ZIP from the parser.
     *
     * @return  Zip or empty string
     */
    protected String zip() {
        String zip = get("zip");
        if (Strings.isNullOrEmpty(zip)) {
            zip = get("listingzip");
        }
        return zip;
    }

    /**
     * Gets the DMA for the supplied ZIP if both exist.
     *
     * @param input ZIP code to map to DMA
     * @return      DMA or empty String
     */
    protected String dma( CharSequence input ) {
        String zip = input.toString();
        String result = "";

        if (!Strings.isNullOrEmpty(zip) && zipToDma.containsKey(zip)) {
            result = zipToDma.get(zip);
        }

        return result;
    }

    /**
     * Create a Session from the fields parsed by the parser.
     *
     * @return      Built Session object
     */
    protected Session buildSession() {
        Session.Builder builder = Session.newBuilder();

        builder.setApiKey( get("apikey") )
                .setResolution( get("res") )
                .setUserId( userId() )
                .setUserAgent( get("uagent") )
                .setActivex( Utils.getActiveX(get("activex")) )
                .setImpressions(buildImpressionList())
                .setLeads(buildLeadList());

        return builder.build();
    }


    /**
     * Build an Impression for this log entry and ad it as the only element to a list.
     *
     * @return  List containing one build Impression object
     */
    protected List<Impression> buildImpressionList() {
        Impression.Builder builder = Impression.newBuilder();
        builder.setAb( get("ab") )
                .setAction(Utils.getAction(get("action")))
                .setActionName(Utils.getActionName(get("action_name")))
                .setAddress(get("address"))
                .setCity(get("city"))
                .setDomain(get("domain"))
                .setId(buildIdList())
                .setImpressionType(Utils.getImpressionType(get("type")))
                .setLat(Utils.getDouble(get("lat")))
                .setLon(Utils.getDouble(get("lon")))
                .setPhoneType(Utils.getPhoneType(get("phone_type")))
                .setState(get("state"))
                .setStartIndex(Utils.getInt(get("start_index")))
                .setTimestamp(Utils.getInt(get("timestamp")))
                .setTotal(Utils.getInt(get("total")))
                .setVertical(Utils.getVertical(get("vertical")))
                .setZip(zip());

        // Set the DMA join based on the ZIP
        builder.setDma(dma(builder.getZip()));

        return Lists.newArrayList(builder.build());
    }

    /**
     * Build an empty Lead list for this Session.
     *
     * @return  Empty List of leads
     */
    protected List<Lead> buildLeadList() {
        return Lists.newArrayList();
    }

    /**
     * Convenience method so I didn't have to type parser.get() so many times.
     *
     * @param k     String field name
     * @return      Stored value or default
     */
    protected String get(String k) {
        return parser.get(k);
    }

    /**
     * Builds a list of Longs from the comma-separated string in the id field.
     *
     * @return List of Longs (sorted)
     */
    private List<Long> buildIdList() {
        Set<Long> longIds = new TreeSet<Long>();

        for( String id : s.split(get("id")) ) {
            longIds.add( Utils.getLong(id) );
        }
        return Lists.newArrayList(longIds);
    }
}
